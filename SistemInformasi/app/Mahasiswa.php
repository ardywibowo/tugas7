<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';

    public function kelas()
    {
        return $this->belongsToMany('App\Kelas');
    }
}
