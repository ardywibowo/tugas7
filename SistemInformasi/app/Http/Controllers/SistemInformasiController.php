<?php

namespace App\Http\Controllers;

use App\Dosen;
use App\Kelas;
use App\Mahasiswa;
use App\MataKuliah;
use App\Riwayat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SistemInformasiController extends Controller
{
    //dosen
    public function dosen()
    {
        $data = Dosen::all();
        return view('dosen.tampildosen', ['data'=>$data]);
    }

    public function tambahdosen()
    {
        return view('dosen.tambahdosen');
    }

    public function simpandosen(Request $request)
    {
        DB::table('dosen')->insert([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'gelar' => $request->gelar
        ]);

        return redirect('/dosen');
    }

    public function editdosen($id)
    {
        $data = Dosen::find($id);
        return view('dosen.editdosen', ['data'=>$data]);
    }

    public function updatedosen(Request $request)
    {
        DB::table('dosen')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'nip' => $request->nip,
            'gelar' => $request->gelar
        ]);

        return redirect('/dosen');
    }

    public function hapusdosen($id)
    {
        DB::table('dosen_matakuliah')->where('dosen_id', $id)->delete();
        DB::table('dosen')->where('id', $id)->delete();

        return redirect('/dosen');
    }

    //riwayat
    public function riwayat($id)
    {
        $data = Dosen::find($id);
        $sudah = DB::select('select * from matakuliah where id = any (select matakuliah_id from dosen_matakuliah where dosen_id = :id)', ['id' => $id]);
        $belum = DB::select('select * from matakuliah where id not in (select matakuliah_id from dosen_matakuliah where dosen_id = :id)', ['id' => $id]);
        return view('dosen.riwayat', ['data'=>$data, 'sudah'=>$sudah, 'belum'=>$belum]);
    }

    public function tambahriwayat($id)
    {
        return view('dosen.tambahriwayat',['id'=>$id]);
    }

    public function simpanriwayat(Request $request)
    {
        DB::table('riwayat_pendidikan')->insert([
            'strata' => $request->strata,
            'jurusan' => $request->jurusan,
            'sekolah' => $request->sekolah,
            'tahun_mulai' => $request->mulai,
            'tahun_selesai' => $request->selesai,
            'dosen_id' => $request->id
        ]);

        return redirect('/dosen/riwayat/'.$request->id);
    }

    public function editriwayat($id)
    {
        $data = Riwayat::find($id);
        return view('dosen.editriwayat', ['data'=>$data]);
    }

    public function updateriwayat(Request $request)
    {
        DB::table('riwayat_pendidikan')->where('id', $request->id)->update([
            'strata' => $request->strata,
            'jurusan' => $request->jurusan,
            'sekolah' => $request->sekolah,
            'tahun_mulai' => $request->mulai,
            'tahun_selesai' => $request->selesai
        ]);

        return redirect('/dosen/riwayat/'.$request->dosen_id);
    }

    public function hapusriwayat($id,$dosen_id)
    {
        DB::table('riwayat_pendidikan')->where('id', $id)->delete();

        return redirect('/dosen/riwayat/'.$dosen_id);
    }

    //mahasiswa
    public function mahasiswa()
    {
        $data = Mahasiswa::all();
        return view('mahasiswa.tampilmahasiswa', ['data'=>$data]);
    }

    public function tambahmahasiswa()
    {
        return view('mahasiswa.tambahmahasiswa');
    }

    public function simpanmahasiswa(Request $request)
    {
        DB::table('mahasiswa')->insert([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'jenis_kelamin' => $request->jenis_kelamin,
            'ttl' => $request->ttl,
            'jumlah_sks'=> 0
        ]);

        return redirect('/mahasiswa');
    }

    public function editmahasiswa($id)
    {
        $data = Mahasiswa::find($id);
        return view('mahasiswa.editmahasiswa', ['data'=>$data]);
    }

    public function updatemahasiswa(Request $request)
    {
        DB::table('mahasiswa')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'nim' => $request->nim,
            'jenis_kelamin' => $request->jenis_kelamin,
            'ttl' => $request->ttl
        ]);

        return redirect('/mahasiswa');
    }

    public function hapusmahasiswa($id)
    {
        DB::table('kelas_mahasiswa')->where('mahasiswa_id', $id)->delete();
        DB::table('mahasiswa')->where('id', $id)->delete();

        return redirect('/mahasiswa');
    }
    public function detailmahasiswa($id)
    {
        $data = Mahasiswa::find($id);
        $sudah = DB::select('select * from kelas where id = any (select kelas_id from kelas_mahasiswa where mahasiswa_id = :id)', ['id' => $id]);
        $belum = DB::select('select * from kelas where id not in (select kelas_id from kelas_mahasiswa where mahasiswa_id = :id)', ['id' => $id]);
        return view('mahasiswa.detailmahasiswa', ['data'=>$data, 'sudah'=>$sudah, 'belum'=>$belum]);
    }

    //matakuliah
    public function matakuliah()
    {
        $data = MataKuliah::all();
        return view('matakuliah.tampilmatakuliah', ['data'=>$data]);
    }

    public function tambahmatakuliah()
    {
        return view('matakuliah.tambahmatakuliah');
    }

    public function simpanmatakuliah(Request $request)
    {
        DB::table('matakuliah')->insert([
            'nama' => $request->nama,
            'sks' => $request->sks
        ]);

        return redirect('/matakuliah');
    }

    public function editmatakuliah($id)
    {
        //$data = DB::table('matakuliah')->where('id', $id)->get();
        $data = MataKuliah::find($id);
        return view('matakuliah.editmatakuliah', ['data'=>$data]);
    }

    public function updatematakuliah(Request $request)
    {
        DB::table('matakuliah')->where('id', $request->id)->update([
            'nama' => $request->nama,
            'sks' => $request->sks
        ]);

        return redirect('/matakuliah');
    }

    public function hapusmatakuliah($id)
    {
        $matakuliah = MataKuliah::find($id);

        foreach ($matakuliah->kelas as $k)
        {
            $kelas = DB::table('kelas_mahasiswa')->where('kelas_id', $k->id)->get();
            foreach ($kelas as $kelas)
            {
                $data = Mahasiswa::find($kelas->mahasiswa_id);
                $jumlah_sks = $data->jumlah_sks;
                $jumlah_sks = $jumlah_sks - $matakuliah->sks;
                DB::table('mahasiswa')->where('id', $kelas->mahasiswa_id)->update([
                    'jumlah_sks' => $jumlah_sks
                ]);
            }
            DB::table('kelas_mahasiswa')->where('kelas_id', $k->id)->delete();
            DB::table('kelas')->where('id', $k->id)->delete();
        }
        DB::table('dosen_matakuliah')->where('matakuliah_id', $id)->delete();
        DB::table('matakuliah')->where('id', $id)->delete();

        return redirect('/matakuliah');
    }

    public function tambahmatakuliahdosen($matakuliah_id,$dosen_id)
    {
        DB::table('dosen_matakuliah')->insert([
            'matakuliah_id' => $matakuliah_id,
            'dosen_id' => $dosen_id
        ]);

        return redirect('/dosen/riwayat/'.$dosen_id);
    }

    public function hapusmatakuliahdosen($matakuliah_id,$dosen_id)
    {
        DB::table('dosen_matakuliah')->where([
            ['matakuliah_id', $matakuliah_id],
            ['dosen_id', $dosen_id],
        ])->delete();

        return redirect('/dosen/riwayat/'.$dosen_id);
    }

    //kelas
    public function kelas($matakuliah_id)
    {
        $data = MataKuliah::find($matakuliah_id);
        return view('dosen.tampilkelas', ['data'=>$data]);
    }

    public function tambahkelas($id)
    {
        return view('dosen.tambahkelas',['id'=>$id]);
    }

    public function simpankelas(Request $request)
    {
        DB::table('kelas')->insert([
            'nama' => $request->nama,
            'matakuliah_id' => $request->id
        ]);

        return redirect('/dosen/matakuliah/kelas/'.$request->id);
    }

    public function editkelas($id)
    {
        //$data = DB::table('kelas')->where('id', $id)->get();
        $data = Kelas::find($id);
        return view('dosen.editkelas', ['data'=>$data]);
    }

    public function updatekelas(Request $request)
    {
        DB::table('kelas')->where('id', $request->id)->update([
            'nama' => $request->nama
        ]);

        return redirect('/dosen/matakuliah/kelas/'.$request->matakuliah_id);
    }

    public function hapuskelas($id,$matakuliah_id)
    {
        $kelas = DB::table('kelas_mahasiswa')->where('kelas_id', $id)->get();
        $matakuliah = MataKuliah::find($matakuliah_id);
        foreach ($kelas as $k)
        {
            $data = Mahasiswa::find($k->mahasiswa_id);
            $jumlah_sks = $data->jumlah_sks;
            $jumlah_sks = $jumlah_sks - $matakuliah->sks;
            DB::table('mahasiswa')->where('id', $k->mahasiswa_id)->update([
                'jumlah_sks' => $jumlah_sks
            ]);
        }

        DB::table('kelas_mahasiswa')->where('kelas_id', $id)->delete();
        DB::table('kelas')->where('id', $id)->delete();

        return redirect('/dosen/matakuliah/kelas/'.$matakuliah_id);
    }

    public function tambahkelasmahasiswa($kelas_id,$mahasiswa_id)
    {
        $data = Mahasiswa::find($mahasiswa_id);
        $jumlah_sks = $data->jumlah_sks;
        $kelas = Kelas::find($kelas_id);
        $jumlah_sks = $jumlah_sks + $kelas->matakuliah->sks;
        if($jumlah_sks > 24)
        {
            return redirect('/mahasiswa/gagal/'.$mahasiswa_id);
        }
        DB::table('mahasiswa')->where('id', $mahasiswa_id)->update([
            'jumlah_sks' => $jumlah_sks
        ]);
        DB::table('kelas_mahasiswa')->insert([
            'kelas_id' => $kelas_id,
            'mahasiswa_id' => $mahasiswa_id
        ]);
        return redirect('/mahasiswa/detail/'.$mahasiswa_id);
    }

    public function hapuskelasmahasiswa($kelas_id,$mahasiswa_id)
    {
        $data = Mahasiswa::find($mahasiswa_id);
        $jumlah_sks = $data->jumlah_sks;
        $kelas = Kelas::find($kelas_id);
        $jumlah_sks = $jumlah_sks - $kelas->matakuliah->sks;
        DB::table('mahasiswa')->where('id', $mahasiswa_id)->update([
            'jumlah_sks' => $jumlah_sks
        ]);

        DB::table('kelas_mahasiswa')->where([
            ['kelas_id', $kelas_id],
            ['mahasiswa_id', $mahasiswa_id],
        ])->delete();

        return redirect('/mahasiswa/detail/'.$mahasiswa_id);
    }

    public function gagalmahasiswa($mahasiswa_id)
    {
        $data = Mahasiswa::find($mahasiswa_id);
        return view('mahasiswa.gagaltambahkelas', ['data'=>$data]);
    }
}
