<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Riwayat extends Model
{
    protected $table = 'riwayat_pendidikan';

    public function dosen()
    {
        return $this->belongsTo('App\Dosen');
    }
}
