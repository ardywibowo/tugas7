<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataKuliah extends Model
{
    protected $table = 'matakuliah';

    public function dosen()
    {
        return $this->belongsToMany('App\Dosen');
    }
    public function kelas()
    {
        return $this->hasMany('App\Kelas', 'matakuliah_id');
    }
}
