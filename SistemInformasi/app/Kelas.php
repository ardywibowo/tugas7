<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';

    public function mahasiswa()
    {
        return $this->belongsToMany('App\Mahasiswa');
    }
    public function matakuliah()
    {
        return $this->belongsTo('App\MataKuliah');
    }
}
