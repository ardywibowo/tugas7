<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';

    public function riwayat_pendidikan()
    {
        return $this->hasMany('App\Riwayat');
    }

    public function matakuliah()
    {
        return $this->belongsToMany('App\MataKuliah');
    }
}
