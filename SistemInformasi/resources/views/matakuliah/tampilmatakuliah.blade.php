<!doctype html>
<html lang="en">
<head>
    <title>List Mata Kuliah</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Mata Kuliah</h2>
        <div class="row p-3">
            <a href="/matakuliah/tambah" class="btn btn-primary col-2 mr-2">Input Matakuliah Baru</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="sks">Jumlah SKS</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->sks }}</td>
                    <td>
                        <a href="/matakuliah/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/matakuliah/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
