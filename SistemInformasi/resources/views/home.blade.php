<!doctype html>
<html lang="en">
<head>
    <title>Home</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Home</h2>
        <div class="row p-3">
            <a href="/matakuliah" class="btn btn-primary col-2 mr-2">Halaman Mata Kuliah</a>
            <a href="/mahasiswa" class="btn btn-primary col-2 mr-2">Halaman Mahasiswa</a>
            <a href="/dosen" class="btn btn-primary col-2 mr-2">Halaman Dosen</a>
        </div>
    </div>
</div>
</body>
