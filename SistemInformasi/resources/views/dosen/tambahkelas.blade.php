<!doctype html>
<html lang="en">
<head>
    <title>Tambah Kelas</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Kelas</h2>
        <form method="post" action="/dosen/matakuliah/kelas/simpan">

            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="form-group">
                <label class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
