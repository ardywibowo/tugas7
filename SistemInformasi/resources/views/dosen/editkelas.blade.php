<!doctype html>
<html lang="en">
<head>
    <title>Edit Kelas</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Kelas</h2>
        <form method="post" action="/dosen/matakuliah/kelas/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data->id }}">
                <input type="hidden" name="matakuliah_id" value="{{ $data->matakuliah_id }}">
                <label>Strata</label>
                <input type="text" name="nama" class="form-control" required value="{{$data->nama}}">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
