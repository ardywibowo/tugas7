<!doctype html>
<html lang="en">
<head>
    <title>Tambah Dosen</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Dosen</h2>
        <form method="post" action="/dosen/simpan">

            {{ csrf_field() }}

            <div class="form-group">
                <label class="form-label">Nama</label>
                <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">NIP</label>
                <input type="number" name="nip" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Gelar</label>
                <input type="text" name="gelar" class="form-control" required>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
