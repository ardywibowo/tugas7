<!doctype html>
<html lang="en">
<head>
    <title>List Dosen</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Dosen</h2>
        <div class="row p-3">
            <a href="/dosen/tambah" class="btn btn-primary col-2 mr-2">Input Dosen Baru</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="nip">NIP</th>
                <th id="gelar">Gelar</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->nip }}</td>
                    <td>{{ $d->gelar }}</td>
                    <td>
                        <a href="/dosen/riwayat/{{ $d->id }}" class="btn btn-primary">Detail</a>
                        <a href="/dosen/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/dosen/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
