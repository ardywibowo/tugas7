<!doctype html>
<html lang="en">
<head>
    <title>Detail Mata Kuliah</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Detail Mata Kuliah</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th><b>Nama</b></th>
                    <td>{{ $data->nama }}</td>
                </tr>
                <tr>
                    <th><b>Jumlah SKS</b></th>
                    <td>{{ $data->sks }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <h2 class="card-title mt-2 mb-3">List Kelas</h2>
        <div class="row p-3">
            <a href="/dosen/matakuliah/kelas/tambah/{{$data->id}}" class="btn btn-primary col-2 mr-2">Input Kelas Baru</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->kelas as $k)
                <tr>
                    <td>{{ $k->id }}</td>
                    <td>{{ $k->nama }}</td>
                    <td>
                        <a href="/dosen/matakuliah/kelas/edit/{{ $k->id }}" class="btn btn-primary">Edit</a>
                        <a href="/dosen/matakuliah/kelas/hapus/{{ $k->id }}/{{$data->id}}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
