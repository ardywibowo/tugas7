<!doctype html>
<html lang="en">
<head>
    <title>Edit Dosen</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Dosen</h2>
        <form method="post" action="/dosen/riwayat/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data->id }}">
                <input type="hidden" name="dosen_id" value="{{ $data->dosen_id }}">
                <label>Strata</label>
                <input type="text" name="strata" class="form-control" required value="{{$data->strata}}">
            </div>
            <div class="form-group">
                <label>Jurusan</label>
                <input type="text" name="jurusan" class="form-control" required value="{{$data->jurusan}}">
            </div>
            <div class="form-group">
                <label>Sekolah</label>
                <input type="text" name="sekolah" class="form-control" required value="{{$data->sekolah}}">
            </div>
            <div class="form-group">
                <label>Tahun Mulai</label>
                <input type="number" name="mulai" class="form-control" required value="{{$data->tahun_mulai}}">
            </div>
            <div class="form-group">
                <label>Tahun Selesai</label>
                <input type="number" name="selesai" class="form-control" required value="{{$data->tahun_selesai}}">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
