<!doctype html>
<html lang="en">
<head>
    <title>Detail Dosen</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Detail Dosen</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th><b>Nama</b></th>
                    <td>{{ $data->nama }}</td>
                </tr>
                <tr>
                    <th><b>NIP</b></th>
                    <td>{{ $data->nip }}</td>
                </tr>
                <tr>
                    <th><b>Gelar</b></th>
                    <td>{{ $data->gelar }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <h2 class="card-title mt-2 mb-3">Riwayat Pendidikan</h2>
        <div class="row p-3">
            <a href="/dosen/riwayat/tambah/{{$data->id}}" class="btn btn-primary col-2 mr-2">Input Riwayat Baru</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="strata">Strata</th>
                <th id="jurusan">Jurusan</th>
                <th id="sekolah">Sekolah</th>
                <th id="tahunmulai">Tahun Mulai</th>
                <th id="tahunselesai">Tahun Selesai</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->riwayat_pendidikan as $r)
                <tr>
                    <td>{{ $r->id }}</td>
                    <td>{{ $r->strata }}</td>
                    <td>{{ $r->jurusan }}</td>
                    <td>{{ $r->sekolah }}</td>
                    <td>{{ $r->tahun_mulai }}</td>
                    <td>{{ $r->tahun_selesai }}</td>
                    <td>
                        <a href="/dosen/riwayat/edit/{{ $r->id }}" class="btn btn-primary">Edit</a>
                        <a href="/dosen/riwayat/hapus/{{ $r->id }}/{{$data->id}}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="card p-3 mt-3">
        <h1 class="card-title mt-2 mb-3">Mata Kuliah Tersedia</h1>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="sks">Jumlah SKS</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($belum as $b)
                <tr>
                    <td>{{ $b->id }}</td>
                    <td>{{ $b->nama }}</td>
                    <td>{{ $b->sks }}</td>
                    <td>
                        <a href="/dosen/matakuliah/tambah/{{ $b->id }}/{{ $data->id }}" class="btn btn-success">Tambah</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="card p-3 mt-3">
        <h2 class="card-title mt-2 mb-3">Mata Kuliah yang diambil</h2>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="sks">Jumlah SKS</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($sudah as $s)
                <tr>
                    <td>{{ $s->id }}</td>
                    <td>{{ $s->nama }}</td>
                    <td>{{ $s->sks }}</td>
                    <td>
                        <a href="/dosen/matakuliah/kelas/{{ $s->id }}" class="btn btn-primary">Detail</a>
                        <a href="/dosen/matakuliah/hapus/{{ $s->id }}/{{ $data->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
