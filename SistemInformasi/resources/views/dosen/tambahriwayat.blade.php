<!doctype html>
<html lang="en">
<head>
    <title>Tambah Riwayat</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Tambah Riwayat</h2>
        <form method="post" action="/dosen/riwayat/simpan">

            {{ csrf_field() }}
            <input type="hidden" name="id" value="{{ $id }}">
            <div class="form-group">
                <label class="form-label">Strata</label>
                <input type="text" name="strata" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Jurusan</label>
                <input type="text" name="jurusan" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Sekolah</label>
                <input type="text" name="sekolah" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Tahun Mulai</label>
                <input type="number" name="mulai" class="form-control" required>
            </div>
            <div class="form-group">
                <label class="form-label">Tahun Selesai</label>
                <input type="number" name="selesai" class="form-control" required>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
