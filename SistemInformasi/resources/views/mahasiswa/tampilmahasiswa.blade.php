<!doctype html>
<html lang="en">
<head>
    <title>List Mahasiswa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">List Mahasiswa</h2>
        <div class="row p-3">
            <a href="/mahasiswa/tambah" class="btn btn-primary col-2 mr-2">Input Mahasiswa Baru</a>
        </div>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="nim">NIM</th>
                <th id="jeniskelamin">Jenis Kelamin</th>
                <th id="ttl">Tempat, Tanggal lahir</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $d)
                <tr>
                    <td>{{ $d->id }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->nim }}</td>
                    <td>{{ $d->jenis_kelamin }}</td>
                    <td>{{ $d->ttl}}</td>
                    <td>
                        <a href="/mahasiswa/detail/{{ $d->id }}" class="btn btn-primary">Detail</a>
                        <a href="/mahasiswa/edit/{{ $d->id }}" class="btn btn-primary">Edit</a>
                        <a href="/mahasiswa/hapus/{{ $d->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
