<!doctype html>
<html lang="en">
<head>
    <title>Edit Mahasiswa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Edit Mahasiswa</h2>
        <form method="post" action="/mahasiswa/update">

            {{ csrf_field() }}

            <div class="form-group">
                <input type="hidden" name="id" value="{{ $data->id }}">
                <label>Nama</label>
                <input type="text" name="nama" class="form-control" required value="{{$data->nama}}">
            </div>
            <div class="form-group">
                <label>NIM</label>
                <input type="text" name="nim" class="form-control" required value="{{$data->nim}}">
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <input type="text" name="jenis_kelamin" class="form-control" required value="{{$data->jenis_kelamin}}">
            </div>
            <div class="form-group">
                <label>Tempat, Tanggal lahir</label>
                <input type="text" name="ttl" class="form-control" required value="{{$data->ttl}}">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>

        </form>
    </div>
</div>
</body>
