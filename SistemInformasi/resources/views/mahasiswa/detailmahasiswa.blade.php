<!doctype html>
<html lang="en">
<head>
    <title>Detail Mahasiswa</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-2 mb-3">Detail Mahasiswa</h2>
        <div class="table-responsive">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th><b>Nama</b></th>
                    <td>{{ $data->nama }}</td>
                </tr>
                <tr>
                    <th><b>NIM</b></th>
                    <td>{{ $data->nim }}</td>
                </tr>
                <tr>
                    <th><b>Jenis Kelamin</b></th>
                    <td>{{ $data->jenis_kelamin }}</td>
                </tr>
                <tr>
                    <th><b>Tempat, Tanggal Lahir</b></th>
                    <td>{{ $data->ttl }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card p-3 mt-3">
        <h1 class="card-title mt-2 mb-3">Kelas Tersedia</h1>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($belum as $b)
                <tr>
                    <td>{{ $b->id }}</td>
                    <td>{{ $b->nama }}</td>
                    <td>
                        <a href="/mahasiswa/kelas/tambah/{{ $b->id }}/{{ $data->id }}" class="btn btn-success">Tambah</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="card p-3 mt-3">
        <h2 class="card-title mt-2 mb-3">Kelas yang diambil</h2>
        <table class="table table-striped mt-3">
            <thead>
            <tr>
                <th id="id">Id</th>
                <th id="nama">Nama</th>
                <th id="opsi">Opsi</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->kelas as $s)
                <tr>
                    <td>{{ $s->id }}</td>
                    <td>{{ $s->nama }}</td>
                    <td>
                        <a href="/mahasiswa/kelas/hapus/{{ $s->id }}/{{ $data->id }}" class="btn btn-danger">Hapus</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
