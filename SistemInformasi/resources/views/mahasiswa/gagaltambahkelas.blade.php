<!doctype html>
<html lang="en">
<head>
    <title>Gagal</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
<div class="container p-3">
    <div class="card p-3">
        <h2 class="card-title mt-3">Gagal Tambah Kelas</h2>
        <h5 class="card-body">Jumlah SKS melebihi 24</h5>
        <a href="/mahasiswa/detail/{{ $data->id }}" class="btn btn-primary col-3">Kembali</a>

    </div>
</div>
</body>
