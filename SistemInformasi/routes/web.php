<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SistemInformasiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

//dosen
Route::get('/dosen',[SistemInformasiController::class, 'dosen']);
Route::get('/dosen/tambah',[SistemInformasiController::class, 'tambahdosen']);
Route::post('/dosen/simpan',[SistemInformasiController::class, 'simpandosen']);
Route::get('/dosen/edit/{id}',[SistemInformasiController::class, 'editdosen']);
Route::post('/dosen/update',[SistemInformasiController::class, 'updatedosen']);
Route::get('/dosen/hapus/{id}',[SistemInformasiController::class, 'hapusdosen']);

Route::get('/dosen/riwayat/{id}',[SistemInformasiController::class, 'riwayat']);
Route::get('/dosen/riwayat/tambah/{id}',[SistemInformasiController::class, 'tambahriwayat']);
Route::post('/dosen/riwayat/simpan',[SistemInformasiController::class, 'simpanriwayat']);
Route::get('/dosen/riwayat/edit/{id}',[SistemInformasiController::class, 'editriwayat']);
Route::post('/dosen/riwayat/update',[SistemInformasiController::class, 'updateriwayat']);
Route::get('/dosen/riwayat/hapus/{id}/{dosen_id}',[SistemInformasiController::class, 'hapusriwayat']);

//mahasiswa
Route::get('/mahasiswa',[SistemInformasiController::class, 'mahasiswa']);
Route::get('/mahasiswa/tambah',[SistemInformasiController::class, 'tambahmahasiswa']);
Route::post('/mahasiswa/simpan',[SistemInformasiController::class, 'simpanmahasiswa']);
Route::get('/mahasiswa/edit/{id}',[SistemInformasiController::class, 'editmahasiswa']);
Route::post('/mahasiswa/update',[SistemInformasiController::class, 'updatemahasiswa']);
Route::get('/mahasiswa/hapus/{id}',[SistemInformasiController::class, 'hapusmahasiswa']);

Route::get('/mahasiswa/detail/{id}',[SistemInformasiController::class, 'detailmahasiswa']);

//matakuliah
Route::get('/matakuliah',[SistemInformasiController::class, 'matakuliah']);
Route::get('/matakuliah/tambah',[SistemInformasiController::class, 'tambahmatakuliah']);
Route::post('/matakuliah/simpan',[SistemInformasiController::class, 'simpanmatakuliah']);
Route::get('/matakuliah/edit/{id}',[SistemInformasiController::class, 'editmatakuliah']);
Route::post('/matakuliah/update',[SistemInformasiController::class, 'updatematakuliah']);
Route::get('/matakuliah/hapus/{id}',[SistemInformasiController::class, 'hapusmatakuliah']);

//dosen matkul
Route::get('/dosen/matakuliah/tambah/{matakuliah_id}/{dosen_id}',[SistemInformasiController::class, 'tambahmatakuliahdosen']);
Route::get('/dosen/matakuliah/hapus/{matakuliah_id}/{dosen_id}',[SistemInformasiController::class, 'hapusmatakuliahdosen']);

//kelas
Route::get('/dosen/matakuliah/kelas/{matakuliah_id}',[SistemInformasiController::class, 'kelas']);
Route::get('/dosen/matakuliah/kelas/tambah/{matakuliah_id}',[SistemInformasiController::class, 'tambahkelas']);
Route::post('/dosen/matakuliah/kelas/simpan',[SistemInformasiController::class, 'simpankelas']);
Route::get('/dosen/matakuliah/kelas/edit/{matakuliah_id}',[SistemInformasiController::class, 'editkelas']);
Route::post('/dosen/matakuliah/kelas/update',[SistemInformasiController::class, 'updatekelas']);
Route::get('/dosen/matakuliah/kelas/hapus/{id}/{matakuliah_id}',[SistemInformasiController::class, 'hapuskelas']);

//mahasiswa kelas
Route::get('/mahasiswa/kelas/tambah/{kelas_id}/{mahasiswa_id}',[SistemInformasiController::class, 'tambahkelasmahasiswa']);
Route::get('/mahasiswa/kelas/hapus/{kelas_id}/{mahasiswa_id}',[SistemInformasiController::class, 'hapuskelasmahasiswa']);
Route::get('/mahasiswa/gagal/{mahasiswa_id}',[SistemInformasiController::class, 'gagalmahasiswa']);
